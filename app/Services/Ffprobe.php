<?php

declare(strict_types=1);

namespace App\Services;

use Illuminate\Support\Arr;

class Ffprobe
{
    /**
     * Get the full data for a file.
     *
     * @param string $filename
     * @param bool $skipTags
     * @return array
     */
    public function data(string $filename): array
    {
        $file = escapeshellarg($filename);
        $com = "ffprobe -v quiet -of json -show_streams -show_format $file";
        $out = [];
        exec($com, $out);

        return $this->normalizeTags(implode("\n", $out));
    }

    protected function normalizeTags(string $output): array
    {
        $data = json_decode($output, true);

        if (empty($data)) {
            return [];
        }

        $tags = Arr::get($data, 'format.tags');

        if (empty($tags)) {
            return $data;
        }

        foreach (array_keys($tags) as $key) {
            $val = Arr::pull($tags, $key);
            $tags[strtolower($key)] = $val;
        }
        Arr::set($data, 'format.tags', $tags);

        return $data;
    }

    public function format(string $filename, bool $skipTags = false)
    {
        $format = Arr::get($this->data($filename), 'format', []);

        return $skipTags ? Arr::except($format, 'tags') : $format;
    }

    /**
     * Returns tags for a file.
     *
     * @param string $filename
     * @return array
     */
    public function tags(string $filename): array
    {
        return Arr::get($this->format($filename), 'tags', []);
    }

    public function streams(string $filename): array
    {
        return Arr::get($this->data($filename), 'streams', []);
    }

    public function streamsByType(string $filename, string $type, bool $first = null): ?array
    {
        $streams = Arr::where($this->streams($filename), fn ($val) => $val['codec_type'] === $type);

        return $first ? Arr::first($streams) : $streams;
    }

    public function video(string $filename, bool $first = null): ?array
    {
        return $this->streamsByType($filename, 'video', $first);
    }

    public function audio(string $filename, bool $first = false): ?array
    {
        return $this->streamsByType($filename, 'audio', $first);
    }
}
