<?php

declare(strict_types=1);

namespace App\Contracts;

use Illuminate\Support\Arr;
use LaravelZero\Framework\Commands\Command;

/**
 * This is to be used for subcommands that will need to return data to their parent command.
 */
abstract class SubCommand extends Command
{
    protected $data = [];
    protected $ret = 0;

    /**
     * Adds a field to the data.
     *
     * May overwrite existing data.
     *
     * @param string $key
     * @param mixed $value
     */
    protected function addData(string $key, $value): void
    {
        Arr::set($this->data, $key, $value);
    }

    /**
     * Gets the data for the parent command.
     *
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * Gets a particular data field.
     *
     * @string $key
     * @return mixed
     */
    public function getField(string $key)
    {
        return Arr::get($this->data, $key);
    }
}
