<?php

declare(strict_types=1);

namespace App\Contracts;

abstract class ConvertCommand extends SubCommand
{
    /**
     * Get the output file for the given input file.
     *
     * @param string $in
     * @param string $infix If in and out are the same, this will be added to the filename
     * @return string
     */
    protected function newFile(string $in, string $infix = 'conv'): string
    {
        $out = preg_replace('/\.([^.]+)$/', '.m4a', $in);
        if ($out === $in) {
            $out = preg_replace('/\.([^.]+)$/', ".{$infix}.m4a", $in);
        }

        return $out;
    }
}
