<?php

declare(strict_types=1);

namespace App\Contracts;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;

abstract class TagCommand extends SubCommand
{
    /**
     * Gets data from tags.
     */
    protected function getTags(array $passed): array
    {
        $r = [];
        foreach ($passed as $key => $value) {
            $r[Str::camel(trim($key))] = trim($value);
        }

        [$track, $trackTotal] = $this->getParts(Arr::get($r, 'track'));
        [$disc, $discTotal] = $this->getParts(Arr::get($r, 'disc'));

        $r['track'] = $track;
        $r['trackTotal'] = $trackTotal;
        $r['disc'] = $disc;
        $r['discTotal'] = $discTotal;

        if ($r['date'] ?? null) {
            $matches = null;
            if (preg_match('/^([0-9]{4})([0-9]{2})([0-9]{2})$/', $r['date'], $matches) && $matches) {
                $r['date'] = "{$matches[1]}-{$matches[2]}-{$matches[3]}";
            }
        }

        return array_filter($r);
    }

    /**
     * Gets parts of a track or disc value.
     */
    protected function getParts(?string $value): array
    {
        $parts = explode('/', $value ?? '');

        return [reset($parts), Arr::get($parts, 1)];
    }
}
