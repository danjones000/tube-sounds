<?php

declare(strict_types=1);

namespace App\Providers;

use App\Services\Ffprobe;
use Danjones\Xattr\Xattr;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Xattr::class);
        $this->app->alias(Xattr::class, 'xattr');

        $this->app->singleton(Ffprobe::class);
        $this->app->alias(Ffprobe::class, 'ffprobe');
    }
}
