<?php

declare(strict_types=1);

namespace App\Commands;

use App\Contracts\TagCommand;
use Illuminate\Support\Arr;

class DropLog extends TagCommand
{
    /** @var string The signature of the command. */
    protected $signature = 'drop-a-log {file}';

    /** @var string The description of the command. */
    protected $description = 'Add the file to your tracks droplog';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $file = $this->argument('file');
        $call = $this->buildCommand($file);

        $ret = 0;
        passthru($call, $ret);

        return $ret;
    }

    protected function buildCommand(string $file): string
    {
        $tags = app('ffprobe')->tags($file);
        $tags = $this->getTags($tags);

        $this->parseNotes($tags);
        $this->tryUrlFromAttr($file, $tags);
        $title = $this->getTitle($tags) ?? $file;

        $this->cleanTags($tags);
        $tags['via'] = 'tube-sounds';

        return sprintf(
            'drop-a-log tracks %s -j %s',
            escapeshellarg($title),
            escapeshellarg(json_encode(array_filter($tags)))
        );
    }

    protected function cleanTags(array &$tags): void
    {
        Arr::pull($tags, 'majorBrand');
        Arr::pull($tags, 'minorVersion');
        Arr::pull($tags, 'compatibleBrands');
        Arr::pull($tags, 'comment');
        Arr::pull($tags, 'description');
        Arr::pull($tags, 'synopsis');

        $tags['release'] = Arr::pull($tags, 'date');

        $notes = Arr::get($tags, 'notes', '');
        $tags['notes'] = strtok($notes, "\n");
    }

    protected function parseNotes(array &$tags): void
    {
        $this->tryUrlFromComments($tags);
        $notes = [
            Arr::pull($tags, 'synopsis'),
            Arr::pull($tags, 'comments'),
            Arr::pull($tags, 'description'),
        ];
        $notes = array_filter($notes);

        if ($notes) {
            $tags['notes'] = reset($notes);
        }
    }

    protected function tryUrlFromComments(array &$tags): void
    {
        $comment = Arr::get($tags, 'comment', '');
        $reg = '/^Downloaded from .+ channel: (http[^,]+)(?:,yt-dl)?$/m';
        $matches = [];
        if (preg_match($reg, $comment, $matches)) {
            $tags['url'] = $matches[1];
            Arr::pull($tags, 'comment');
        }
    }

    protected function tryUrlFromAttr(string $file, array &$tags): void
    {
        if ($url = app('xattr')->get($file, 'xdg.referrer.url')) {
            $tags['url'] = $url;
        }
    }

    protected function getTitle(array &$tags): ?string
    {
        $title = Arr::pull($tags, 'title');
        if (!$title) {
            return null;
        }

        $tags['song'] = $title;
        $artist = Arr::get($tags, 'artist');

        return $title . (
            $artist ?
            " by $artist" :
            ''
        );
    }
}
