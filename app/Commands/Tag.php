<?php

declare(strict_types=1);

namespace App\Commands;

use App\Contracts\TagCommand;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class Tag extends TagCommand
{
    /** @var string The signature of the command. */
    protected $signature = 'tag {file} ' .
                         '{--A|album=}' .
                         '{--a|artist=} ' .
                         '{--d|disk=} ' .
                         '{--D|disks= : Total number of discs} ' .
                         '{--g|genre=} ' .
                         '{--M|episode=} ' .
                         '{--o|season=} ' .
                         '{--R|albumartist=} ' .
                         '{--s|song=} ' .
                         '{--S|show=} ' .
                         '{--t|track=} ' .
                         '{--T|tracks= : Total number of tracks on disc} ' .
                         '{--w|writer=} ' .
                         '{--y|year=}';

    /** @var string The description of the command. */
    protected $description = 'Tags the given file with mp4tags';

    protected $tags = [
        'song',
        'album',
        'artist',
        'writer',
        'albumartist',
        'track' => ['1', '-'],
        'tracks' => ['1', '-'],
        'disk' => ['1', '-'],
        'disks' => ['1', '-'],
        'year',
        'genre',
        'episode',
        'season',
        'show',
    ];

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $file = $this->argument('file');
        $tags = app('ffprobe')->tags($file);
        $data = $this->getTags($tags);

        $args = $this->askTags($data);
        $com = "mp4tags $args " . escapeshellarg($file);
        $ret = 0;
        passthru($com, $ret);

        return $ret;
    }

    /**
     * Ask for the tags from the user, if needed.
     *
     * @param array $data
     * @return string Command-line arguments for mp4tags
     */
    protected function askTags(array $data): string
    {
        $r = '';
        foreach ($this->tags as $name => $parts) {
            if (is_int($name)) {
                $name = $parts;
                $parts = [];
            }

            $found = $data[$this->getTagName($name)] ?? null;

            if ($name === 'albumartist' && !$found) {
                $found = $data['artist'] ?? null;
            }

            $args = [$found, $name, ...$parts];
            $value = $this->getTag(...$args);
            if ($value) {
                $r .= " -{$name} " . escapeshellarg($value);
            }
        }

        return $r;
    }

    /**
     * Gets the ffprobe tag name for the mp4tags tag name.
     */
    protected function getTagName(string $optName): string
    {
        if ($optName === 'song') {
            return 'title';
        }

        if ($optName === 'writer') {
            return 'composer';
        }

        if ($optName === 'year') {
            return 'date';
        }

        if ($optName === 'albumartist') {
            return 'albumArtist';
        }

        if ($optName === 'tracks') {
            return 'trackTotal';
        }

        if ($optName === 'disk') {
            return 'disc';
        }

        if ($optName === 'disks') {
            return 'discTotal';
        }

        return $optName;
    }

    /**
     * Gets the value, asking user if necessary.
     */
    protected function getTag(
        ?string $found,
        string $option,
        string $default = null,
        string $nullString = null
    ): ?string {
        $passed = $this->option($option);
        if ($passed) {
            return $passed;
        }

        $question = Str::title($option);
        if ($nullString) {
            $question .= " ('{$nullString}' to unset)";
        }
        $answer = $this->ask($question, $found ?: $default);
        if ($nullString && $answer === $nullString) {
            return null;
        }

        return $answer ?: $default;
    }
}
