<?php

declare(strict_types=1);

namespace App\Commands;

use App\Contracts\ConvertCommand;
use Symfony\Component\Console\Input\StringInput;

class Convert extends ConvertCommand
{
    /** @var string The signature of the command. */
    protected $signature = 'convert ' .
                         '{file : File to convert} ' .
                         '{--t|trash : Trash the original file} ';


    /** @var string The description of the command. */
    protected $description = 'Convert file to m4a';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $file = $this->argument('file');
        $args = strval(new StringInput($this->ask('Additional command-line arguments?') ?? ''));
        $out = $this->convert($file, $args);

        $this->line($out);
        $this->addData('file', $out);

        return $this->ret;
    }

    /**
     * Convert the given file using ffmpeg.
     *
     * @param string $file
     * @param string $args Additional properly escaped command-line arguments
     * @return string new filename
     */
    protected function convert(string $file, string $args = ''): string
    {
        $out = $this->newFile($file);

        $call = sprintf(
            'ffmpeg -i %s -vn -sn -map_chapters -1 %s %s',
            escapeshellarg($file),
            $args,
            escapeshellarg($out)
        );
        $ret = 0;

        passthru($call, $ret);
        $this->ret += $ret;
        app('xattr')->clone($file, $out);

        return $out;
    }
}
