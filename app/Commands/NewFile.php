<?php

declare(strict_types=1);

namespace App\Commands;

use App\Contracts\SubCommand;

class NewFile extends SubCommand
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'new';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Picks a random file from the YouTube directory';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        chdir(env('YOUTUBE_PATH'));
        $files = $this->buildFileList();
        $idx = random_int(1, count($files));

        $file = $files[$idx - 1];

        $this->line(realpath($file));
        $this->addData('file', realpath($file));
    }

    protected function buildFileList($path = '.'): array
    {
        $cwd = getcwd();
        chdir($path);
        $files = [];
        foreach (glob('*.mp4') as $mp4) {
            $files[] = "${path}/${mp4}";
        }
        foreach (glob('*') as $file) {
            if (is_dir($file)) {
                $additional = $this->buildFileList($file);
                foreach ($additional as $newFile) {
                    $files[] = "${path}/${newFile}";
                }
            }
        }
        chdir($cwd);

        return $files;
    }
}
