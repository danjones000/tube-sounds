<?php

declare(strict_types=1);

namespace App\Commands;

use LaravelZero\Framework\Commands\Command;

class Play extends Command
{
    /** @var string The signature of the command. */
    protected $signature = 'play {file : Filename to play}';

    /** @var string The description of the command. */
    protected $description = 'Run file in external player';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $player = escapeshellarg(env('PLAYER', 'mplayer'));
        $file = escapeshellarg($this->argument('file'));
        $ret = -1;
        passthru("$player $file", $ret);

        return $ret;
    }
}
