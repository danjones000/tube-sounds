<?php

declare(strict_types=1);

namespace App\Commands;

use App\Contracts\SubCommand;

class Trash extends SubCommand
{
    /** @var string The signature of the command. */
    protected $signature = 'trash {file : Filename to move} ' .
                         '{--i|interactive : Run interactively}';

    /** @var string The description of the command. */
    protected $description = 'Trash file';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $file = $this->argument('file');

        $confirm = $this->option('interactive') ? $this->confirm("Do you want to trash $file", true) : true;
        if (!$confirm) {
            return 0;
        }

        $this->ret += $this->trash($file);

        return $this->ret;
    }

    protected function trash(string $filename): int
    {
        $ret = 0;
        $_ = [];
        exec('trash-put ' . escapeshellarg($filename), $_, $ret);

        return $ret;
    }
}
