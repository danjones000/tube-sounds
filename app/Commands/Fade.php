<?php

declare(strict_types=1);

namespace App\Commands;

use App\Contracts\ConvertCommand;

class Fade extends ConvertCommand
{
    /** @var string The signature of the command. */
    protected $signature = 'fade ' .
                         '{file : File to fade in/out} ' .
                         '{length? : Length of the resulting file} ' .
                         '{--in= : Fade in length} ' .
                         '{--out= : Fade out length} ' .
                         '{--t|trash : Trash the original file} ' .
                         '{--k|keep : Keep the temporary file}';

    /** @var string The description of the command. */
    protected $description = 'Fade in/out file';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $file = $this->argument('file');
        $length = $this->getValue($this->argument('length'), 'How long should the total length of the file be?');
        $in = $this->getValue($this->option('in'), 'How long should the fade in be?');
        $out = $this->getValue($this->option('out'), 'How long should the fade out be?');

        $tempWav = $this->getWav($file);
        $fadedWav = $this->fadeWav($tempWav, $length, $in, $out);
        $finalFile = $this->convertFadedWav($file, $fadedWav);

        if (!$this->option('keep')) {
            unlink($tempWav);
            unlink($fadedWav);
        } else {
            $this->line("Temporary wav: $tempWav");
            $this->line("Faded wav: $fadedWav");
        }

        if ($this->option('trash')) {
            exec(sprintf(
                'trash-put %s 2>/dev/null',
                escapeshellarg($file)
            ));
        }

        $this->addData('file', $finalFile);
        $this->line($finalFile);

        return $this->ret;
    }

    /**
     * Get an optional numeric value, prompting the user if missing.
     *
     * @param string|null $passed Value to be returned
     * @param string $question
     * @return int
     */
    protected function getValue(?string $passed, string $question): int
    {
        if (!$passed || !is_numeric($passed)) {
            $passed = $this->ask($question);
        }

        return intval($passed);
    }

    /**
     * Gets a temporary wav file from the mp4, using ffmpeg.
     *
     * @param string $file Input filename
     * @return string Output filename
     */
    protected function getWav(string $file): string
    {
        $tempFileName = sys_get_temp_dir() . '/tube-to-fade-' . uniqid() . '.wav';
        $in = escapeshellarg($file);
        $out = escapeshellarg($tempFileName);
        $r = -1;

        passthru("ffmpeg -i $in -vn $out", $r);
        if ($r !== 0) {
            throw new \RuntimeException("Failed to convert $file to $tempFileName");
        }

        return $tempFileName;
    }

    /**
     * Fades in/out the given wav file, using sox.
     *
     * @param string $file Input filename
     * @param int $length total length (including fades) of resulting file
     * @param int $fadeIn the length of the fade in
     * @param int $fadeOut the length of the fade out
     * @return string Output filename
     */
    protected function fadeWav(string $file, int $length, int $fadeIn, int $fadeOut): string
    {
        $tempFileName = sys_get_temp_dir() . '/tube-faded-' . uniqid() . '.wav';
        $in = escapeshellarg($file);
        $out = escapeshellarg($tempFileName);
        $r = -1;

        passthru("sox $in $out fade $fadeIn $length $fadeOut", $r);
        if ($r !== 0) {
            throw new \RuntimeException("Failed to create faded file from $file");
        }

        return $tempFileName;
    }

    /**
     * Convert the faded wav file to an aac file.
     *
     * Also Copy metadata and file attributes from original file.
     *
     * @param string $origFile The original unmodified file
     * @param string $wav file created by {fadeWav}.
     * @return string the new file
     */
    protected function convertFadedWav(string $origFile, string $wav): string
    {
        $out = $this->newFile($origFile, 'faded');

        $call = sprintf(
            'ffmpeg -i %s -i %s -vn -sn -map 1 -map_metadata 0 -map_metadata:s:a 0:s:a -map_chapters -1 %s',
            escapeshellarg($origFile),
            escapeshellarg($wav),
            escapeshellarg($out)
        );
        $ret = 0;

        passthru($call, $ret);
        $this->ret += $ret;

        app('xattr')->clone($origFile, $out);

        return $out;
    }
}
