<?php

declare(strict_types=1);

namespace App\Commands;

use App\Contracts\SubCommand;

class Noop extends SubCommand
{
    /** @var string The signature of the command. */
    protected $signature = 'noop';

    /** @var string The description of the command. */
    protected $description = 'Do nothing';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        return 0;
    }
}
