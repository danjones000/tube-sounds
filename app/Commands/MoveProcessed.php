<?php

declare(strict_types=1);

namespace App\Commands;

use App\Contracts\SubCommand;
use voku\helper\ASCII;

class MoveProcessed extends SubCommand
{
    /** @var string The signature of the command. */
    protected $signature = 'mv-proc ' .
                         '{--i|interactive : Run interactively}';

    /** @var string The description of the command. */
    protected $description = 'Move processed files to music folder';

    protected $replacements = [
        '℃-ute' => 'Cute',
    ];

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $dest = env('DEST_PATH');
        $music = env('MUSIC_PATH');

        if (!is_dir($dest)) {
            $this->error('Invalid destination directory: ' . $dest);
            return 1;
        }
        if (!is_dir($music)) {
            $this->error('Invalid music directory: ' . $music);
            return 1;
        }

        chdir($dest);
        $files = glob('*.m4a');
        array_push($files, ...glob('*.mp3'));

        foreach ($files as $file) {
            $dest = $this->getDest($file, $music);
            $confirm = $this->option('interactive') ? $this->confirm("Do you want to move $file to $dest", true) : true;
            if (!$confirm) {
                continue;
            }
            if (!is_dir(dirname($dest))) {
                mkdir(dirname($dest), 0775, true);
            }
            $ret = rename($file, $dest);
            if (!$ret) {
                $this->error("Failed to move $file to $dest");
                return 1;
            }

            $this->line("Moved $file to $dest");
        }

        return $this->ret;
    }

    protected function getTrackNumber(array $tags): ?string
    {
        $episode = $tags['season_number'] ?? '';
        if ($episode) {
            $episode .= 'x' . ($tags['episode_sort'] ?? '');
        }

        $track = $tags['track'] ?? '';
        $trackParts = explode('/', $track);
        $track = reset($trackParts);
        $trackTotal = $trackParts[1] ?? null;

        if ($track === "1" && $trackTotal === "1") {
            $track = null;
        }

        $disc = $tags['disc'] ?? null;
        if ($track && $disc) {
            $discParts = explode('/', $disc);
            $disc = reset($discParts);
            $discTotal = $discParts[1] ?? null;

            if ($disc && $discTotal && $discTotal > 1) {
                $track = sprintf('d%02dt%02d', $disc, $track);
            }
        }

        return $track ?: $episode ?: null;
    }

    /**
     * Gets the destination filename, based on metadata.
     */
    protected function getDest(string $file, string $dest): string
    {
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        $tags = app('ffprobe')->tags($file);
        $artist = $tags['album_artist'] ?? $tags['artist'] ?? 'Unknown';
        $album = $tags['album'] ?? 'Unknown';
        $track = $this->getTrackNumber($tags);

        $title = $tags['title'] ?? 'Unknown';

        $i = 0;
        do {
            $ret = sprintf(
                '%s/%s/%s/%s',
                $dest,
                $this->safe($artist),
                $this->safe($album),
                sprintf(
                    '%s%s%s.%s',
                    $track ? (is_numeric($track) ? sprintf('%02d', $track) : $track) . '-' : '',
                    $this->safe($title),
                    $i > 0 ? '-' . sprintf('%02d', $i) : '',
                    $ext
                )
            );
            $i++;
        } while (file_exists($ret));

        return $ret;
    }

    protected function safe(string $str): string
    {
        if (array_key_exists($str, $this->replacements)) {
            $str = $this->replacements[$str];
        }

        return ASCII::to_filename($str, true, '_');
    }
}
