<?php

declare(strict_types=1);

namespace App\Commands;

use LaravelZero\Framework\Commands\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class Info extends Command
{
    /** @var string The signature of the command. */
    protected $signature = 'info {file}';

    /** @var string The description of the command. */
    protected $description = 'Gets info for the given file';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $file = $this->argument('file');
        $data = app('ffprobe')->format($file, true);
        $tags = app('ffprobe')->tags($file);

        $rows = [
            ['Filename', Arr::pull($data, 'filename', $this->argument('file'))],
        ];
        $this->addIf($tags, 'title', $rows);
        $this->addIf($tags, 'artist', $rows);
        $this->addIf($tags, 'album', $rows);
        $this->addIf($tags, 'genre', $rows);
        $this->addIf($tags, 'track', $rows);

        Arr::forget($data, ['probe_score', 'start_time']);
        Arr::forget($tags, ['description', 'comment', 'synopsis']);

        foreach ($tags as $k => $_) {
            $this->addIf($tags, $k, $rows);
        }
        foreach ($data as $k => $_) {
            $this->addIf($data, $k, $rows);
        }

        $this->table(['', ''], $rows);

        return 0;
    }

    protected function addIf(array &$source, string $key, array &$dest, string $name = null): void
    {
        $name = $name ?? Str::title(str_replace('_', ' ', $key));
        if ($val = Arr::pull($source, $key)) {
            $print = is_iterable($val) ? json_encode($val) : strval($val);
            $dest[] = [$name, $print];
        }

        if ($val = Arr::pull($source, strtoupper($key))) {
            $print = is_iterable($val) ? json_encode($val) : strval($val);
            $dest[] = [$name, $print];
        }
    }
}
