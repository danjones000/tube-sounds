<?php

declare(strict_types=1);

namespace App\Commands;

use App\Contracts\SubCommand;

class Move extends SubCommand
{
    /** @var string The signature of the command. */
    protected $signature = 'mv {file : Filename to move} ' .
                         '{--t|trash=* : Also trash these files} ' .
                         '{--i|interactive : Run interactively}';

    /** @var string The description of the command. */
    protected $description = 'Move file to destination';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $file = $this->argument('file');
        $dest = env('DEST_PATH');

        if (!is_dir($dest)) {
            $this->error('Invalid destination directory: ' . $dest);
            return 1;
        }

        $base = basename($file);
        $destFile = $dest . DIRECTORY_SEPARATOR . $base;

        $confirm = $this->option('interactive') ? $this->confirm("Do you want to move $file to $destFile", true) : true;
        if (!$confirm) {
            return 0;
        }

        $ret = rename($file, $destFile);

        if (!$ret) {
            $this->error("Failed to move $file to $destFile");
            return 1;
        }

        $trashed = [];
        foreach ($this->option('trash') as $toTrash) {
            $confirm = $this->option('interactive') ? $this->confirm("Do you want to trash $toTrash", true) : true;
            if ($confirm) {
                $this->ret += $this->trash($toTrash);
                $trashed[] = $toTrash;
            }
        }

        $this->addData('file.original', $file);
        $this->addData('file.destination', $destFile);
        $this->addData('trashed', $trashed);

        return $this->ret;
    }

    protected function trash(string $filename): int
    {
        $ret = 0;
        $_ = [];
        exec('trash-put ' . escapeshellarg($filename), $_, $ret);

        return $ret;
    }
}
