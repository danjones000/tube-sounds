<?php

declare(strict_types=1);

namespace App\Commands;

use App\Contracts\SubCommand;

class FindFile extends SubCommand
{
    /** @var string The signature of the command. */
    protected $signature = 'find';

    /** @var string The description of the command. */
    protected $description = 'Find a file using fuzzy search';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $cwd = getcwd();
        chdir(env('YOUTUBE_PATH'));
        $out = exec('fd .mp4 | fzf');
        $out = realpath(trim($out));
        $this->line($out);
        $this->addData('file', $out);

        chdir($cwd);

        return 0;
    }
}
