<?php

declare(strict_types=1);

namespace App\Commands;

use App\Contracts\ConvertCommand;
use App\Contracts\SubCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Arr;
use LaravelZero\Framework\Commands\Command;
use LaravelZero\Framework\Providers\CommandRecorder\CommandRecorderRepository;

class Run extends Command
{
    /** @var string The signature of the command. */
    protected $signature = 'run';

    /** @var string The description of the command. */
    protected $description = 'Make Music from YouTube downloads';

    protected $currentFile = null;
    protected $toTrash = [];

    protected $options = [];

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $this->start();
        $option = $this->doMenu();

        while (!is_null($option)) {
            $command = $this->getCommand($option);
            $arguments = $this->getArgs($option);

            $command = $this->resolveCommand($command);
            $ret = $command->run($this->createInputFromArguments($arguments), $this->output);

            if (($cb = $this->getCallback($option)) && ($ret === 0)) {
                $cb($command);
            } elseif ($ret !== 0) {
                $this->error('Command failed');
                $this->wait();
            }
            $option = $this->doMenu();
        }

        return 0;
    }

    protected function doMenu(): ?string
    {
        return $this
            ->menu('What would you like to do?')
            ->enableAutoShortcuts()
            ->addOptions($this->getOptions())
            ->open();
    }

    /**
     * Get the options for the menu.
     */
    protected function getOptions(): array
    {
        $keep = Arr::except($this->options, ['arguments', 'callback']);
        $keys = array_keys($keep);
        $values = Arr::pluck($keep, 'label');

        return array_combine($keys, $values);
    }

    /**
     * Get the command class for a given option.
     */
    protected function getCommand(string $option): string
    {
        return Arr::get($this->options, "{$option}.class", Noop::class);
    }

    /**
     * Get the arguments to pass to the command.
     */
    protected function getArgs(string $option): array
    {
        return Arr::get($this->options, "{$option}.arguments", Arr::get($this->options, 'arguments', []));
    }

    /**
     * Get the callback after a successful run of the command.
     */
    protected function getCallback(string $option): ?callable
    {
        return Arr::get($this->options, "{$option}.callback", Arr::get($this->options, 'callback'));
    }

    /**
     * Begin command.
     */
    protected function start(): void
    {
        $this->currentFile = null;
        $this->toTrash = [];
        $this->options = [
            'new' => [
                'label'    => 'Get a new [r]andom file',
                'class'    => NewFile::class,
            ],
            'find' => [
                'label'    => '[F]ind a file using fuzzy search',
                'class'    => FindFile::class,
            ],
            'mv-proc' => [
                'label'     => 'Mo[v]e processed music files',
                'class'     => MoveProcessed::class,
                'callback'  => [$this, 'start'],
                'arguments' => [
                    '--interactive' => true,
                ],
            ],
            'callback' => [$this, 'newFile'],
        ];
    }

    /**
     * Process the currently chosen file.
     */
    protected function processFile(): void
    {
        $dir = dirname($this->currentFile);
        $current = basename($dir) . '/' . basename($this->currentFile);

        $this->options = [
            'play' => [
                'label' => "[P]lay $current",
                'class' => Play::class,
                'arguments' => ['file' => $this->currentFile],
                'callback' => [$this, 'wait'],
            ],
            'info' => [
                'label' => "[I]nfo about $current",
                'class' => Info::class,
                'arguments' => ['file' => $this->currentFile],
                'callback' => [$this, 'wait'],
            ],
            'conv' => [
                'label' => "[C]onvert $current in ffmpeg",
                'class' => Convert::class,
                'arguments' => ['file' => $this->currentFile],
                'callback' => [$this, 'conv'],
            ],
            'fade' => [
                'label' => "Create a [f]aded copy of $current",
                'class' => Fade::class,
                'arguments' => ['file' => $this->currentFile],
                'callback' => [$this, 'conv'],
            ],
            'tag' => [
                'label' => "[T]ag $current",
                'class' => Tag::class,
                'arguments' => ['file' => $this->currentFile],
            ],
            'droplog' => [
                'label' => "[D]rop tracks log for $current",
                'class' => DropLog::class,
                'arguments' => ['file' => $this->currentFile],
                'callback' => [$this, 'wait'],
            ],
            'move' => [
                'label' => "Mo[v]e $current and delete other files",
                'class' => Move::class,
                'arguments' => [
                    'file' => $this->currentFile,
                    '--interactive' => true,
                    '--trash' => $this->toTrash,
                ],
                'callback' => function (Move $command) {
                    $this->wait();
                    $this->start();
                },
            ],
            'trash' => [
                'label' => "T[r]ash $current",
                'class' => Trash::class,
                'arguments' => [
                    'file' => $this->currentFile,
                    '--interactive' => true,
                ],
                'callback' => function (Trash $command) {
                    $this->wait();
                    $this->start();
                },
            ],
            'new' => [
                'label' => 'Pick [n]ew song',
                'callback' => [$this, 'start'],
            ],
        ];
    }

    /**
     * Get converted file.
     */
    protected function conv(ConvertCommand $command)
    {
        $this->toTrash[] = $this->currentFile;
        $this->currentFile = $command->getField('file');
        $this->processFile(); // Just to refresh name
    }

   /**
     * Prints the output and waits for the user to hit return to continue.
     */
    protected function wait(): void
    {
        $this->ask('Press Enter to continue');
    }

    /**
     * Select $file as the current file.
     *
     * @param string $file
     */
    protected function newFile(SubCommand $command): void
    {
        $file = $command->getField('file');
        $this->currentFile = trim($file);
        $this->line('Got this:');
        $this->line($this->currentFile ?: 'nothing');

        $this->processFile();
    }
}
